/**
 * threadpool.c
 *
 * A work-stealing, fork-join thread pool.
 */

#include "./threadpool.h"
#include <semaphore.h>
#include <stdlib.h>
#include <err.h>

#define FATAL(string) (err(1, string" at %s:%d: ", __FILE__, __LINE__))

//forward declarations
struct future * future_init(void);
void future_resolve(struct future * future, void * result);

//definitions

struct future {
    void * result;
    sem_t * control;
    void * funct;
    int status;
    void *args;

};

struct thread_pool {
	struct list worker_threads;
	struct list global_task_pool;

};

void * future_get(struct future * future){
    //wait for result
    sem_wait(future->control);
    return future->result;
};

void future_free(struct future * future){
    if (sem_destroy(future->control))
        FATAL("future_free sem_destroy failed");
    free(future->control);
    free(future->result);
    free(future);
};

struct future * future_init(){
    struct future * future = malloc(sizeof(struct future));
    future->control = malloc(sizeof(sem_t));
    if (future->control == NULL)
        err(1, "future_init malloc failed: ");
    if (sem_init(future->control, 0, 0))
        FATAL("future_init sem_init failed");
    return future;
};

struct thread_pool * thread_pool_new(int nthreads) {
	struct thread_pool * thread_pool = malloc(sizeof(struct thread_pool));

	//spawn threads
	int i = 0;
	for(; i < nthreads; i++) {
		if(pthread_create(&thread_pool[i], NULL, &worker_entry_point, NULL)) {
			printf("Could not create worker thread\n");
			pthread_exit(NULL);
		}
	}

	return thread_pool;
}


void future_resolve(struct future * future, void * result){
    future->result = result;
    if (sem_post(future->control))
        FATAL("future_resolve sem_post failed");
};

void event_loop() {
	//While thread pool is not empty,
	//Take a task from the task pool, assign it to a worker,
	//allow a worker to operate

}

void worker_entry_point() {
	//This is for the worker thread logic


	//If no work tasks in global or steal queues then exit.

}


