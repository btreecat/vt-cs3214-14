struct future;
struct future * future_init(void);
void future_resolve(struct future * future, void * result);
void * future_get(struct future * future);
void future_free(struct future * future);
