#!/bin/bash

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR=$(dirname $0)

for test in $(ls $DIR/*_test.o); do
    valgrind -q --error-exitcode=2 $test
    case $? in
        0) echo [PASS] $test;;
        1) echo [FAIL] $test threw errors;;
        2) echo [FAIL] $test had memory problems;;
    esac
done

