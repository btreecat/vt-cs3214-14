#include "../future.h"

int main(int argc, char *argv[]) {
    struct future * future = future_init();
    future_free(future);
    return 0;
}
